import os
import vcf

def variant_analysis(sample, database, output_dir, vcf_files):
    '''variant annotating using snpEff'''
    variant_analysis_files = os.path.join(output_dir, "variant_analysis")
    variant_analysis_summary_files = os.path.join(output_dir, "variant_analysis_summary")

    if not os.path.exists(variant_analysis_files):
        os.mkdir(variant_analysis_files)
    if not os.path.exists(variant_analysis_summary_files):
        os.mkdir(variant_analysis_summary_files)

    vcf_input = os.path.join(vcf_files, f"{sample}.vcf")
    if os.path.exists(vcf_input):
        vcf_reader = vcf.Reader(filename=vcf_input)
        count = 0
        for _ in vcf_reader:
            count += 1
        if count != 0:
            summary_output = os.path.join(variant_analysis_summary_files,
                                            f"{sample}_snpEff_summary.html")
            vcf_output = os.path.join(variant_analysis_files, f"{sample}.eff.vcf")
            os.system(f"snpEff eff -stats {summary_output} {database} {vcf_input} > {vcf_output}")