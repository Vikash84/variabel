import os
from collections import defaultdict
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import numpy as np
import pysam
import random

def error_profiling(sample_id):
    output_bases = ["a", "A", "c", "C", "g", "G", "t", "T", "*"]

    bam_file_path = "/home/Users/yl181/nano_variant_caller/time_series/nanopore/bam_files"
    samfile = pysam.AlignmentFile(os.path.join(bam_file_path, f"{sample_id}_sort.bam"), "rb")

    base_count_f = open(f"{sample_id}.basecount.txt", "w")
    base_count_f.write(f"#POS\tREF\tDP\t")
    output_bases_header = "\t".join(output_bases)
    base_count_f.write(f"{output_bases_header}\tINS\tDEL\n")

    for pileupcolumn in samfile.pileup(max_depth=80000):
        
        base_dict = defaultdict(int)
        ins_dict = defaultdict(int)
        del_dict = defaultdict(int)
        
        ref_pos = pileupcolumn.pos
        ref_base = reference_seq[ref_pos]
        depth = pileupcolumn.n
        
        aligned_bases = pileupcolumn.get_query_sequences(mark_matches=False, 
                                                         add_indels=True, 
                                                         mark_ends=False)
        
        for base in aligned_bases:
            # base count
            base_dict[base[0]] += 1
            
            # indels
            if len(base) > 1:
                # insertions
                if base[1] == "+":
                    # insertion with length less than 10 bases
                    if len(base) <= 12:
                        ins_length = base[2]
                        ins_dict[base[3:]] += 1
                    # insertion with length equal or greater than 10 bases
                    elif len(base) >= 14:
                        ins_length = base[2:3]
                        ins_dict[base[4:]] += 1
                    # should never be here
                    elif len(base) == 13:
                        print("Error:", base)
                
                # deletions
                if base[1] == "-":
                    # deletion with length less than 10 bases
                    if len(base) <= 12:       
                        del_length = base[2]
                        del_dict[base[3:]] += 1
                    # deletion with length equal or greater than 10 bases
                    elif len(base) >= 14:     
                        del_length = base[2:3]
                        del_dict[base[4:]] += 1
                    # should never be here
                    elif len(base) == 13:
                        print("Error:", base)
        
        if(False):
            print(f"{ref_pos}\t{ref_base}\t{depth}", end="\t")
            for base in output_bases:
                print(base_dict[base], end="\t")

            ins_list = []
            for ins in ins_dict:
                ins_list.append(f"{ins}:{ins_dict[ins]}")
            print(",".join(ins_list), end="\t")

            del_list = []
            for deletion in del_dict:
                del_list.append(f"{deletion}:{del_dict[deletion]}")
            print(",".join(del_list), end="\t")
            print()

        base_count_f.write(f"{ref_pos}\t{ref_base}\t{depth}\t")
        for base in output_bases:
            base_count_f.write(f"{base_dict[base]}\t")
        ins_list = []
        for ins in ins_dict:
            ins_list.append(f"{ins}:{ins_dict[ins]}")
        ins_str = ",".join(ins_list)
        base_count_f.write(f"{ins_str}\t")
        del_list = []
        for deletion in del_dict:
            del_list.append(f"{deletion}:{del_dict[deletion]}")
        del_str = ",".join(del_list)
        base_count_f.write(f"{del_str}\n")

    base_count_f.close()

# reference_genome_file = "../NC_045512.2.fasta"
# reference_seq= SeqIO.read(reference_genome_file, "fasta").seq   

# metafile = "../metadata.csv"

# with open(metafile, "r") as metadata:
#     for line in metadata.readlines()[1:]:
#         nanopore_id = line.strip().split(",")[2]
#         print(nanopore_id)
#         error_profiling(nanopore_id)