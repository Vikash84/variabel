import os
import subprocess

def data_retriving(sample_metadata, read_dir):
    '''download datasets from NCBI'''
    if not os.path.exists(read_dir):
        os.mkdir(read_dir)
    for run in sample_metadata:
        if not os.path.exists(os.path.join(read_dir, run)):
            subprocess.run(["prefetch", run],
                           check=True)
            if not os.path.exists(os.path.join(read_dir, run)):
                os.mkdir(os.path.join(read_dir, run))
            subprocess.run([
                "fastq-dump",
                "--split-3",
                "--outdir", os.path.join(read_dir, run), run],
                           check=True)

def read_mapping_bwa(sample, reference_file, num_cores, output_dir, read_dir):
    '''map the reads to the reference'''
    sam_output_dir = os.path.join(output_dir, "sam_files")
    if not os.path.exists(sam_output_dir):
        os.mkdir(sam_output_dir)

    subprocess.run([
        "bwa",
        "mem",
        "-t", str(num_cores),
        "-o", os.path.join(sam_output_dir, f"{sample}.sam"),
        reference_file,
        os.path.join(read_dir, sample, f"{sample}_1.fastq"),
        os.path.join(read_dir, sample, f"{sample}_2.fastq")],
                    stdout=open(os.path.join(output_dir, "simulation.log"), "a"),
                    stderr=open(os.path.join(output_dir, "simulation.err"), "a"),
                    check=True)

def read_mapping_minimap2(sample, reference_file, num_cores, output_dir, read_dir):
    '''map the reads to the reference'''
    sam_output_dir = os.path.join(output_dir, "sam_files")
    if not os.path.exists(sam_output_dir):
        os.mkdir(sam_output_dir)

    subprocess.run([
        "minimap2",
        "-ax", "map-ont",
        "-t", str(num_cores),
        "-o", os.path.join(sam_output_dir, f"{sample}.sam"),
        reference_file,
        os.path.join(read_dir, sample, f"{sample}.fastq")],
                    stdout=open(os.path.join(output_dir, "simulation.log"), "a"),
                    stderr=open(os.path.join(output_dir, "simulation.err"), "a"),
                    check=True)

def sort_samfile(sample, output_dir, num_cores):
    '''converting and sorting alignment files'''
    bam_files = os.path.join(output_dir, "bam_files")
    sam_files = os.path.join(output_dir, "sam_files")
    if not os.path.exists(bam_files):
        os.mkdir(bam_files)

    # covert sam file to binary bam file
    subprocess.run([
        "samtools",
        "view",
        "-@", str(num_cores),
        "-bS", os.path.join(sam_files, f"{sample}.sam"),
        "-o", os.path.join(bam_files, f"{sample}.bam")],
                    check=True)
    # sort the bam file for variant caller
    subprocess.run([
        "samtools",
        "sort",
        "-@", str(num_cores),
        "-o", os.path.join(bam_files, f"{sample}_sort.bam"),
        "-O", "BAM", os.path.join(bam_files, f"{sample}.bam")],
                    check=True)

    # indexing the sorted bam file for variant caller
    subprocess.run([
        "samtools",
        "index",
        os.path.join(bam_files, f"{sample}_sort.bam")],
                    check=True)

def run_fastp(read_1, read_2, output_prefix, output_dir, log_dir,
              num_cores,
              s_window_size, s_window_q_threshold,
              min_len,
              correction=False,
              base_q_threshold=15, unqualified_limit=40,
              n_base_limit=5,
              low_complexity_threshold=30,
              no_html=True):
    if not os.path.exists(output_dir):
            os.mkdir(output_dir)
            
    '''trim reads using fastp'''
    if correction:
        subprocess.run(["fastp",
                        "--in1", read_1, "--in2", read_2,
                        "--out1", os.path.join(output_dir, f"{output_prefix}_1.fastq"),
                        "--out2", os.path.join(output_dir, f"{output_prefix}_2.fastq"),
                        "--detect_adapter_for_pe",
                        "--cut_front",
                        "--cut_window_size", str(s_window_size),
                        "--cut_mean_quality", str(s_window_q_threshold),
                        "--qualified_quality_phred", str(base_q_threshold),
                        "--unqualified_percent_limit", str(unqualified_limit),
                        "--n_base_limit", str(n_base_limit),
                        "--length_required", str(min_len),
                        "--low_complexity_filter", "--low_complexity_filter", str(low_complexity_threshold),
                        "--correction",
                        "--json", os.path.join(log_dir, f"{output_prefix}.fastp.log"),
                        "--html", os.path.join(log_dir, f"{output_prefix}.fastp.html"),
                        "--report_title", output_prefix,
                        "--thread", str(num_cores)],
                       check=True)
    else:
        subprocess.run(["fastp",
                        "--in1", read_1, "--in2", read_2,
                        "--out1", os.path.join(output_dir, f"{output_prefix}_1.fastq"),
                        "--out2", os.path.join(output_dir, f"{output_prefix}_2.fastq"),
                        "--detect_adapter_for_pe",
                        "--cut_front",
                        "--cut_window_size", str(s_window_size),
                        "--cut_mean_quality", str(s_window_q_threshold),
                        "--qualified_quality_phred", str(base_q_threshold),
                        "--unqualified_percent_limit", str(unqualified_limit),
                        "--n_base_limit", str(n_base_limit),
                        "--length_required", str(min_len),
                        "--low_complexity_filter", "--low_complexity_filter", str(low_complexity_threshold),
                        "--json", os.path.join(log_dir, f"{output_prefix}.fastp.log"),
                        "--html", os.path.join(log_dir, f"{output_prefix}.fastp.html"),
                        "--report_title", output_prefix,
                        "--thread", str(num_cores)],
                       check=True)

    if no_html:
        if os.path.exists(os.path.join(log_dir, f"{output_prefix}.fastp.html")):
            os.remove(os.path.join(log_dir, f"{output_prefix}.fastp.html"))

raw_read_dir = "/home/Users/yl181/nano_variant_caller/corona_hit/raw_reads"
num_cores = 20
reference_file = "../NC_045512.2.fasta"

sample_metadata = []
with open("/home/Users/yl181/nano_variant_caller/corona_hit/corona_hit_metadata.csv", "r") as metadata:
    for line in metadata.readlines()[1:]:
        _, _, _, sample_id_nanopore, sample_id_illumina = line.strip().split(",")
        if sample_id_nanopore != ".":
            sample_metadata.append(sample_id_nanopore.strip())
        if sample_id_illumina != ".":
            sample_metadata.append(sample_id_illumina.strip())

print(f"Total number of samples: {len(sample_metadata)}")
# data_retriving(sample_metadata, raw_read_dir)

# alignment
for sample in sample_metadata:
    read_1 = os.path.join(raw_read_dir, sample, f"{sample}_1.fastq")
    read_2 = os.path.join(raw_read_dir, sample, f"{sample}_2.fastq")
    single_end_read = os.path.join(raw_read_dir, sample, f"{sample}.fastq")

    if os.path.exists(read_1) and os.path.exists(read_2):
        output_dir = "illumina"
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)

        trimmed_reads_dir = os.path.join(output_dir, "trimmed_reads")
        if not os.path.exists(trimmed_reads_dir):
            os.mkdir(trimmed_reads_dir)
        fastp_log_dir = os.path.join(output_dir, "fastp_log")
        if not os.path.exists(fastp_log_dir):
            os.mkdir(fastp_log_dir)        

        run_fastp(read_1, read_2, sample, os.path.join(trimmed_reads_dir, sample), fastp_log_dir,
              num_cores,
              s_window_size=4, s_window_q_threshold=15,
              min_len=15)

        read_mapping_bwa(sample, reference_file, num_cores, output_dir, trimmed_reads_dir)
        sort_samfile(sample, output_dir, num_cores)

    elif os.path.exists(single_end_read):
        output_dir = "nanopore"
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        read_mapping_minimap2(sample, reference_file, num_cores, output_dir, raw_read_dir)
        sort_samfile(sample, output_dir, num_cores)
    else:
        print("Error:", sample)

    print(sample)
    

