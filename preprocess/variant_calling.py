import os
import subprocess

def variant_call_lofreq_illumina(sample_metadata, reference_file, output_dir, num_cores, indel=True):
    '''variant calling using loFreq'''
    bam_files = os.path.join(output_dir, "bam_files")
    vcf_files = os.path.join(output_dir, "vcf_files_lofreq")
    if not os.path.exists(vcf_files):
        os.mkdir(vcf_files)

    if (indel):
        indel_bam_files = os.path.join(output_dir, "indel_bam_files")
        if not os.path.exists(indel_bam_files):
            os.mkdir(indel_bam_files)

        for i, sample in enumerate(sample_metadata):
            if not os.path.exists(os.path.join(vcf_files, f"{sample}.vcf")):
                print(sample, i, "indelqual")
                if os.path.exists(os.path.join(indel_bam_files, f"{sample}_sort.bam")):
                    os.remove(os.path.join(indel_bam_files, f"{sample}_sort.bam"))

                subprocess.run([
                    "lofreq",
                    "indelqual",
                    "--dindel",
                    "-f", f"{reference_file}",
                    "-o", os.path.join(indel_bam_files, f"{sample}_sort.bam"),
                    os.path.join(bam_files, f"{sample}_sort.bam")],
                            stdout=open(os.path.join(output_dir, "simulation.log"), "a"),
                            stderr=open(os.path.join(output_dir, "simulation.err"), "a"),
                            check=True)

                subprocess.run([
                    "samtools", "index",
                    os.path.join(indel_bam_files, f"{sample}_sort.bam")])
                    
                print(sample, i, "call-parallel")
                if os.path.exists(os.path.join(vcf_files, f"{sample}.vcf")):
                    os.remove(os.path.join(vcf_files, f"{sample}.vcf"))

                subprocess.run([
                    "lofreq",
                    "call-parallel",
                    "--pp-threads", str(num_cores),
                    "--call-indels",
                    "-f", f"{reference_file}",
                    "-o",
                    os.path.join(vcf_files, f"{sample}.vcf"),
                    os.path.join(indel_bam_files, f"{sample}_sort.bam")],
                            stdout=open(os.path.join(output_dir, "simulation.log"), "a"),
                            stderr=open(os.path.join(output_dir, "simulation.err"), "a"),
                            check=True)

def variant_call_lofreq_nanopore(sample_metadata, reference_file, output_dir, num_cores, indel=True):
    '''variant calling using loFreq'''
    bam_files = os.path.join(output_dir, "bam_files")
    vcf_files = os.path.join(output_dir, "vcf_files_lofreq")
    if not os.path.exists(vcf_files):
        os.mkdir(vcf_files)

    if (indel):
        indel_bam_files = os.path.join(output_dir, "indel_bam_files")
        if not os.path.exists(indel_bam_files):
            os.mkdir(indel_bam_files)

        for i, sample in enumerate(sample_metadata):
            if not os.path.exists(os.path.join(vcf_files, f"{sample}.vcf")):
                print(sample, i, "indelqual")
                if os.path.exists(os.path.join(indel_bam_files, f"{sample}_sort.bam")):
                    os.remove(os.path.join(indel_bam_files, f"{sample}_sort.bam"))

                subprocess.run([
                    "lofreq",
                    "indelqual",
                    "--uniform", "16",
                    "-f", f"{reference_file}",
                    "-o", os.path.join(indel_bam_files, f"{sample}_sort.bam"),
                    os.path.join(bam_files, f"{sample}_sort.bam")],
                            stdout=open(os.path.join(output_dir, "simulation.log"), "a"),
                            stderr=open(os.path.join(output_dir, "simulation.err"), "a"),
                            check=True)

                if os.path.exists(os.path.join(bam_files, f"{sample}_sort.bam")):
                    os.remove(os.path.join(bam_files, f"{sample}_sort.bam"))
                
                subprocess.run([
                    "samtools", "index",
                    os.path.join(indel_bam_files, f"{sample}_sort.bam")])

                print(sample, i, "call-parallel")
                # if os.path.exists(os.path.join(vcf_files, f"{sample}.vcf")):
                #     os.remove(os.path.join(vcf_files, f"{sample}.vcf"))

                subprocess.run([
                    "lofreq",
                    "call-parallel",
                    "--pp-threads", str(num_cores),
                    "--call-indels",
                    "-f", f"{reference_file}",
                    "-o",
                    os.path.join(vcf_files, f"{sample}.vcf"),
                    os.path.join(indel_bam_files, f"{sample}_sort.bam")],
                            stdout=open(os.path.join(output_dir, "simulation.log"), "a"),
                            stderr=open(os.path.join(output_dir, "simulation.err"), "a"),
                            check=True)

raw_read_dir = "/home/Users/yl181/nano_variant_caller/corona_hit/raw_reads"
num_cores = 40
reference_file = "../NC_045512.2.fasta"

sample_metadata_illumina = []
sample_metadata_nanopore = []
with open("/home/Users/yl181/nano_variant_caller/corona_hit/corona_hit_metadata.csv", "r") as metadata:
    for line in metadata.readlines()[1:]:
        _, _, _, sample_id_nanopore, sample_id_illumina = line.strip().split(",")
        if sample_id_nanopore != ".":
            sample_metadata_nanopore.append(sample_id_nanopore.strip())
        if sample_id_illumina != ".":
            sample_metadata_illumina.append(sample_id_illumina.strip())

print(f"Total number of nanopore samples: {len(sample_metadata_nanopore)}")
output_dir = "nanopore"
variant_call_lofreq_nanopore(sample_metadata_nanopore, reference_file, output_dir, num_cores)

print(f"Total number of illumina samples: {len(sample_metadata_illumina)}")
output_dir = "illumina"
variant_call_lofreq_illumina(sample_metadata_illumina, reference_file, output_dir, num_cores)