#!/usr/bin/python

import sys
import argparse
import os
import shutil
import vcf
import csv

import matplotlib.pyplot as plot

from typing import NamedTuple
from collections import defaultdict

from multiprocessing import Pool
from Bio import SeqIO
from Bio.Seq import Seq

class VariantIdentity(NamedTuple):
    pos: int = 0
    ref: str = 0
    alt: str = 0
    
class VariantInfo:
    nanopore_count: int = 0
    illumina_count: int = 0
    af_total: int = 0
    
class ScatterPlotInfo:
    xs = []
    ys = []
    sizes = []
    afs = []
    count = 0
    
    def __init__(self, x, y, s, af, c):
        self.xs = x
        self.ys = y
        self.sizes = s
        self.afs = af
        self.count = c
    
def get_illumina_names(metadata_file):
    illumina_names = set()

    with open(metadata_file, "r") as file:
        for row in csv.DictReader(file):
            name = row["illumina_id"].strip()
            
            if name != ".":
                illumina_names.add(name)
                
    return illumina_names

def get_variants(var_dir, illumina_names):
    file_names = [triple[2] for triple in os.walk(var_dir)][0]
    vars = defaultdict(lambda: VariantInfo())

    for file_name in file_names:
        vcf_file = os.path.join(var_dir, file_name)
        reader = vcf.Reader(open(vcf_file, "r"))
        
        for record in reader:
            name = os.path.splitext(os.path.basename(vcf_file))[0].split(".")[0]
            pos = record.POS
            ref = str(record.REF)
            af = record.INFO["AF"]
            
            if record.is_indel:
                continue
            
            for alt in [str(x) for x in record.ALT]:
                id = VariantIdentity(pos, ref, alt)
                vars[id].af_total += af
                
                if name in illumina_names:
                    vars[id].illumina_count += 1
                else:
                    vars[id].nanopore_count += 1
                    
    return vars

def plot_variants(vars, out_file, title):
    blue = [0/255, 116/255, 217/255]
    red = [255/255, 65/255, 54/255]
    purple = [177/255, 13/255, 201/255]

    both_info = ScatterPlotInfo([], [], [], [], 0)
    nanopore_info = ScatterPlotInfo([], [], [], [], 0)
    illumina_info = ScatterPlotInfo([], [], [], [], 0)
        
    # Get plot data
    for (id, data) in vars.items():
        total = data.illumina_count + data.nanopore_count
        y = data.af_total / total
        size = 1.5 * total
        color = None
        info = None
        
        if data.nanopore_count != 0 and data.illumina_count != 0:
            info = both_info
            color = purple
        elif data.nanopore_count != 0:
            info = nanopore_info
            color = red
        else:
            info = illumina_info
            color = blue
            
        info.xs.append(id.pos)
        info.ys.append(y)
        info.sizes.append(size)
        info.afs.append(data.af_total)
        info.count += total
        
    figure, axes = plot.subplots()
    figure.set_size_inches(8, 8)
    figure.dpi = 400

    # Plot the data on three scatter plots
    scatter1 = axes.scatter(both_info.xs, both_info.ys, s= both_info.sizes, color= purple, label= "Nanopore and illumina")
    scatter2 = axes.scatter(nanopore_info.xs, nanopore_info.ys, s= nanopore_info.sizes, color= red, label= "Nanopore only")
    scatter3 = axes.scatter(illumina_info.xs, illumina_info.ys, s= illumina_info.sizes, color= blue, label= "Illumina only")
    
    # Plot the nanopore and illumina average lines
    both_mean = sum(both_info.afs) / both_info.count
    axes.hlines(both_mean, 0, 30000, colors= purple, linestyles= "dashed", label= "Nanopore and illumina average frequency")
    
    # Plot the nanopore average lines
    nanopore_mean = sum(nanopore_info.afs) / nanopore_info.count
    axes.hlines(nanopore_mean, 0, 30000, colors= red, linestyles= "dashed", label= "Nanopore average frequency")
    
    # Plot the average lines
    illumina_mean = sum(illumina_info.afs) / illumina_info.count
    axes.hlines(illumina_mean, 0, 30000, colors= blue, linestyles= "dashed", label= "Illumina average frequency")
    
    # Plot the color legend
    leg1 = axes.legend(loc= "lower right", bbox_to_anchor= (1, -0.47))
    axes.add_artist(leg1)
    
    # Plot the size legend
    handles, labels = scatter3.legend_elements(prop= "sizes", num= 6)
    leg2 = axes.legend(handles, labels, loc= "lower left", bbox_to_anchor= (0, -0.595), title= "Number of \nSupporting Samples")
    leg2._legend_box.align = "left"
    
    plot.title(title)
    plot.xlabel("Position")
    plot.ylabel("Average Frequency (2% min)")
    
    figure.subplots_adjust(bottom= 0.35)

    plot.savefig(out_file)
    #plot.show()

    
def run():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('input', help='the input vcf directory')
    parser.add_argument('-m', dest= "metadata", help='the input vcf directory')
    parser.add_argument('-o', dest= "output", help='the output png file')
    parser.add_argument('-e', dest= "title", help='the title of the plot', default= "Frequency and Source of SNPs in Covid Time-Series Data")

    args = parser.parse_args()
    input_dir = args.input
    out_file = args.output
    metadata_file = args.metadata
    title = args.title
    
    illumina_names = get_illumina_names(metadata_file)
    vars = get_variants(input_dir, illumina_names)
    
    plot_variants(vars, out_file, title)
    
run()
