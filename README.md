# Variabel

Infectious disease monitoring on Oxford Nanopore Technologies (ONT) platforms offers rapid turnaround times and low cost, exemplified by over a half of million ONT SARS-COV-2 datasets. However, given the higher error rate of ONT, accurate identification of intrahost variants with low allele frequencies remains an open challenge. In response to this challenge, we present Variabel, a novel approach and method for intrahost variant detection, which outperforms existing ONT variant callers.


## Installing Variabel
Variabel is a python script which requires python>=3.7, Biopython, pyvcf, and scipy. For additional filtering using LoFreq, users must have LoFreq installed as well. Variabel can be installed via Bioconda. The install process should only take a few minutes.
```
conda install variabel
```

Alternatively, the `variabel` script can be downloaded and used directly, as it is a standalone script.

### System requirements
Variabel was tested on Ubuntu 18, however it should be able to run on most operating systems so long as they have python 3.7 or greater. The versions of the packages used for testing are listed below:
* Python 3.7.8
* scipy 1.5.3
* pyvcf 0.6.8
* Biopython 1.79




## Running Variabel
Variabel requires 2 types of input:
1. A list of 1 or more VCF files, where the filenames are formatted as `<SAMPLE_NAME>.vcf`
2. A fasta reference genome

all output will be produced in `variabel_out/` unless an alternative is specified with the `-o` flag.
Please see the help documentation for other parameters.

```
>$ ./variabel -h
usage: variabel [-h] [-o OUTPUT_DIR] [--min-af-change DELTA] [--min-samp-count COUNT] [-f MIN_FREQ] [-k K]
                [--min-entropy-cutoff CUTOFF] [--lofreq-filter] [--verbose]
                input-vcfs [input-vcfs ...] ref

Welcome to Variabel! Variabel applies a complexity filter and an allele frequency filter to VCF files to eliminate noise and retain
true variants. See below for options.

positional arguments:
  input-vcfs            The input vcf file(s). If single input .txt is provided instead, it is expected to be a list of vcf file
                        paths.
  ref                   The reference genome to use (fasta format)

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT_DIR, --out OUTPUT_DIR
                        The output vcf directory
  --min-af-change DELTA
                        The minimum observed allele frequency change required to keep a variant
  --min-samp-count COUNT
                        The minimum number of samples a variant must appear in
  -f MIN_FREQ, --safe-freq-thresh MIN_FREQ
                        Any variant occuring in a sample with AF greater than this threshold will not be discarded in the AF filtering
                        step
  -k K, --complexity-k K
                        Window for shannon entropy calculation. For an indel at position i with length d, the two entropy windows will
                        be [i-(k-1)*d] and [i+1, i+k*d+1]
  --min-entropy-cutoff CUTOFF
                        The shannon entropy below which indel variants are discarded
  --lofreq-filter       Preprocess VCF files with LoFreq's set of defaults filters. This includes strand bias.
  --verbose             More verbose output
```

### Demo run
To run variabel on a sample dataset included in this repo:

```
./variabel demo_data/input/*.vcf demo_data/NC_045512.2.fasta -o demo_output
```

Should produce an output folder `demo_output/` containing the filtered VCF files. The expected output of this comman can be found in `demo_data/expected_output/`
