#!/usr/bin/python

import sys
import argparse
import os
import shutil
import vcf
import math

import matplotlib.pyplot as plot
import scipy.stats as stats

from typing import NamedTuple
from collections import defaultdict

class VariantIdentity(NamedTuple):
    pos: int = 0
    ref: str = 0
    alt: str = 0
    
def get_variants(vcf_file):
    vars = set()
    
    with open(vcf_file, "r") as file:
        reader = vcf.Reader(file)
        
        for record in reader:
            name = os.path.splitext(os.path.basename(vcf_file))[0].split(".")[0]
            pos = record.POS
            ref = str(record.REF)
            
            for alt in [str(x) for x in record.ALT]:
                id = VariantIdentity(pos, ref, alt)
                
                vars.add(id)
                    
    return vars
                    
def run():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('expected', help='a vcf file containing the expected variants')
    parser.add_argument('observed', help='a vcf file containing the observed variants')

    args = parser.parse_args()
    expected_file = args.expected
    observed_file = args.observed
    
    expected = get_variants(expected_file)
    observed = get_variants(observed_file)
    
    ppv = len(expected.intersection(observed)) / len(observed)
    recall = len(expected.intersection(observed)) / len(expected)
    
    print("Precision: " + str(round(ppv, 3)))
    print("Recall:    " + str(round(recall, 3)))
    
run()
