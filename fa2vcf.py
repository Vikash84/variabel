#!/usr/bin/python3

# fa2vcf.py
# Written by Joshua Kearney

# TODO:
# 1. Add snps support
# 2. Finish adding type hints
# 3. Confirm pos column is accurate
# 4. Confirm initial deletions are accurate

import csv
import argparse

from Bio import AlignIO
from py_linq import Enumerable
from typing import List
from collections import namedtuple


IndelIndex = namedtuple("IndelIndex", "ref_index alt_index")
IndelRecord = namedtuple("IndelRecord", "start_pos end_pos ref alt samples")
GapInterval = namedtuple("GapInterval", "start end")


def find_gaps(genome) -> List[GapInterval]:
    results = []
    pos = 0

    while pos < len(genome):
        if genome[pos] == '-':
            start = pos

            while pos < len(genome) and genome[pos] == '-':
                pos += 1

            results.append(GapInterval(start, pos))

        pos += 1

    return results


def calculate_ref_pos(align_pos: int, gaps: List[GapInterval]) -> int:
    if align_pos < 0:
        raise ValueError('align_pos: index out of bounds')

    total_gaps = Enumerable(gaps) \
        .where(lambda x: x.start < align_pos) \
        .select(lambda x: x.end - x.start) \
        .sum()

    return align_pos - total_gaps + 1


def find_deletions(genomes: List[str], ref_i: int, alt_i: int, ref_gaps: List[GapInterval],
                   alt_gaps: List[GapInterval]) -> List[IndelRecord]:

    result = []
    ref_genome = genomes[ref_i]
    alt_genome = genomes[alt_i]

    for (start, end) in alt_gaps:
        pos = calculate_ref_pos(start, ref_gaps)

        # If this deletion is at the beginning of the genome, the reference is every deleted base and one base
        # after. If this deletion is in the middle, the reference is every deleted base and one base before. The
        # alternate is similar
        if start == 0:
            ref = ref_genome[0:end + 1]
            alt = alt_genome[end]
        else:
            ref = ref_genome[start - 1:end]
            alt = alt_genome[start - 1]

        if "-" in ref or "-" in alt:
            raise ValueError('Cannot calculate complex indels. Try aligning only two genomes at once.')

        result.append(IndelRecord(pos, pos+(end-start), ref, alt, [IndelIndex(ref_i, alt_i)]))

    return result


def find_indels(genomes: List[str]) -> List[IndelRecord]:
    gaps = {}
    deletions: List[IndelRecord] = []
    insertions = []

    # Calculate gaps for all genomes
    for i in range(0, len(genomes)):
        gaps[i] = find_gaps(genomes[i])

    # Find insertions and deletions
    for i in range(1, len(genomes)):
        deletions += find_deletions(genomes, 0, i, gaps[0], gaps[i])
        insertions += find_deletions(genomes, i, 0, gaps[i], gaps[0])

    # Flip the alt and ref columns for the insertions because they were calculated as deletions
    insertions = Enumerable(insertions) \
        .where(lambda x: IndelRecord(x.start_pos, x.end_pos, x.alt, x.ref, [x.samples[0].alt_index, x.samples[0].ref_index])) \
        .to_list()

    # Combine deletions that are compatible
    deletions_dict = {}

    for del1 in deletions:
        key = (del1.start_pos, del1.end_pos, del1.ref)

        if key in deletions_dict:
            for samp in del1.samples:
                deletions_dict[key].samples.append(samp)
        else:
            deletions_dict[key] = del1

    deletions = list(deletions_dict.values())

    # Combine both sets of indels
    deletions += insertions

    # Make sure all of our indels are with respect to the reference
    if Enumerable(deletions).any(lambda x: Enumerable(x.samples).any(lambda y: y.ref_index != 0)):
        raise ValueError('Invalid indel found, cannot continue')

    return deletions


def write_indels(genomes, indels, file):
    counter = 0

    for record in indels:
        chrom = str(1)
        pos = record.start_pos
        id = "indel" + str(counter)
        ref = record.ref.upper()
        alt = record.alt.upper()
        qual = "40"
        filter = "INDEL"
        info = "."

        row = [chrom, pos, id, ref, alt, qual, filter, info]

        for i in range(0, len(genomes)):
            if Enumerable(record.samples).any(lambda x: x.alt_index == i):
                row.append(1)
            else:
                row.append(0)

        file.writerow(row)
        counter += 1


def run():
    parser = argparse.ArgumentParser(description='fa2vcf.py: extracts snps and indels from a multi-alignment fasta')
    parser.add_argument('input', help='the multi-alignment fasta file')
    parser.add_argument('-o', dest="output", default="fa2vcf.out.vcf",
                        help='The output vcf file')

    args = parser.parse_args()

    alignment = AlignIO.read(open(args.input), "fasta")
    genomes = [str(x.seq) for x in alignment]

    with open(args.output, "w") as f:
        f.write("##fileformat=VCFv4.2\n")
        f.write("##INFO=<ID=INDEL,Number=0,Type=Flag,Description=\"Indicates that the variant is an INDEL.\">\n")
        f.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\t");

        for name in [str(x.id) for x in alignment]:
            f.write(name + "\t")

        f.write("\n")

    with open(args.output, "a") as f:
        writer = csv.writer(f, delimiter='\t')
        write_indels(genomes, find_indels(genomes), writer)


run()
