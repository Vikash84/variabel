#!/usr/bin/python

# filter_af_change.py
# Written by Joshua Kearney

import sys
import argparse
import os
import shutil
import vcf
import csv
import math

from typing import NamedTuple
from collections import defaultdict
from datetime import datetime
from multiprocessing import Pool

class VariantIdentity(NamedTuple):
    pos: int = 0
    ref: str = 0
    alt: str = 0
                
def get_sample_ids(metadata_file):
    with open(metadata_file, "r") as file:
        reader = csv.DictReader(file)
        samples = []
        
        for row in reader:
            id = row["nanopore_id"].strip()
        
            if id != ".":
                samples.append(id)
                
        return samples


def get_name(path):
    return os.path.splitext(os.path.basename(path))[0].split(".")[0].split("_")[0]


def get_file_ids(vcf_dir):
    file_paths = {}
    
    for file_path in [triple[2] for triple in os.walk(vcf_dir)][0]:
        name = get_name(file_path)
        file_paths[name] = os.path.join(vcf_dir, file_path)
        
    return file_paths


def get_variants(vcf_file):
    vars = defaultdict(lambda: [])
    
    with open(vcf_file, "r") as file:
        reader = vcf.Reader(file)
        
        for record in reader:
            name = os.path.splitext(os.path.basename(vcf_file))[0].split(".")[0]
            pos = record.POS
            ref = str(record.REF)
            
            for alt in [str(x) for x in record.ALT]:
                id = VariantIdentity(pos, ref, alt)
                
                vars[id].append(record)
                    
    return vars

   
def get_all_variants(var_dir, sample_ids):
    var_list = []
    file_paths = get_file_ids(var_dir)
        
    for id in sample_ids:
        path = file_paths[id]
        vars = get_variants(path)
        
        var_list.append(vars)
    
    return var_list


def filter_unsupported_variants(variants, min_count):
    var_counts = defaultdict(int)
    
    for var_dict in variants:
        for (id, records) in var_dict.items():
            var_counts[id] += len(records)
            
    for var_dict in variants:
        for (id, records) in list(var_dict.items()):
            if var_counts[id] < min_count:
                var_dict.pop(id)
         
         
def filter_af(variants, min_variance, min_cov):
    var_freqs = {}
    var_counts = {}
    
    keys = set()
    for var_dict in variants:
        for key in var_dict.keys():
            keys.add(key)
            
    for key in keys:
        var_freqs[key] = []
        var_counts[key] = []
        
        for var_dict in variants:
            if key in var_dict:
                records = var_dict[key]
                mean_af = sum([record.INFO["AF"] for record in records]) / len(records)
                var_freqs[key].append(mean_af)
                var_counts[key].append(sum([record.INFO["AF"] * record.INFO["DP"] for record in records]))
            else:
                var_freqs[key].append(0)
                var_counts[key].append(0)
    
    kept_vars = set()
    for (id, l) in var_freqs.items():
        n = [x for x in l if x != 0]
        
        if max(n) - min(n) >= min_variance or sum([1 for i in n if i >= min_cov]):
            kept_vars.add(id)
            
    for var_dict in variants:
        for key in list(var_dict.keys()):
            if key not in kept_vars:
                var_dict.pop(key)
                
                
def write_vcf(vcf_dir, out_dir, sample_ids, vars):
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    file_paths = {}
    for file_path in [triple[2] for triple in os.walk(vcf_dir)][0]:
        name = os.path.splitext(os.path.basename(file_path))[0].split(".")[0]
        file_paths[name] = os.path.join(vcf_dir, file_path)
    
    for i in range(0, len(sample_ids)):
        sample_name = sample_ids[i]
        var_dict = vars[i]
        in_file = file_paths[sample_name]
        out_file = os.path.join(out_dir, sample_name + ".vcf")
                
        with open(in_file, "r") as file1:
            with open(out_file, "w") as file2:
                reader = vcf.Reader(file1)
                writer = vcf.Writer(file2, reader)
                
                for records in var_dict.values():
                    for record in records:
                        writer.write_record(record)
                    
                    
def run():
    parser = argparse.ArgumentParser(description='Filter variants in the given vcf directory based on the minimum change in allele frequency across samples.')
    parser.add_argument('input', help='the input vcf directory')
    parser.add_argument('--meta', dest= "metadata", help="A metadata file containing a 'nanopore_id' column")
    parser.add_argument('--out', dest= "output", help='the output vcf directory')
    parser.add_argument('--min-af-change', dest= "delta", type= float, default=0.05, help='The minimum observed allele frequency change required to keep a variant')
    parser.add_argument('--min-samp-count', dest= "count", type= float, default=1, help='The minimum number of samples a variant must appear in')
    parser.add_argument('--safe-samp-cov', dest= "min_cov", type= float, default=0.65, help='The minimum coverage needed for a variant to never be removed by filtering')

    args = parser.parse_args()
    input_dir = args.input
    output_dir = args.output
    metadata_file = args.metadata
    
    ids = get_sample_ids(metadata_file)
    vars = get_all_variants(input_dir, ids)
        
    filter_unsupported_variants(vars, args.count)
    filter_af(vars, args.delta, args.min_cov)
    
    write_vcf(input_dir, output_dir, ids, vars)
    
run()
