#!/usr/bin/python

# plot_filter_comparison.py
# Written by Joshua Kearney

import sys
import argparse
import os
import shutil
import itertools
import csv
import vcf

import matplotlib.pyplot as plot

from multiprocessing import Pool
from collections import defaultdict
from typing import NamedTuple
from typing import Dict

from Bio import SeqIO
from Bio.Seq import Seq

class VariantIdentity(NamedTuple):
    pos: int = 0
    ref: str = 0
    alt: str = 0
    
def get_variants(vcf_file):
    vars = set()
    
    with open(vcf_file, "r") as file:
        reader = vcf.Reader(file)
        
        for record in reader:
            name = os.path.splitext(os.path.basename(vcf_file))[0].split(".")[0]
            pos = record.POS
            ref = str(record.REF)
            
            for alt in [str(x) for x in record.ALT]:
                id = VariantIdentity(pos, ref, alt)
                
                vars.add(id)
                    
    return vars
    
def calculate_ppv_recall(vcf_expected, vcf_actual):
    expected = get_variants(vcf_expected)
    observed = get_variants(vcf_actual)
    
    ppv = len(expected.intersection(observed)) / len(observed) if len(observed) > 0 else 0
    recall = len(expected.intersection(observed)) / len(expected) if len(expected) > 0 else 0
    f_score = 2*ppv*recall/(ppv + recall)
    
    return (ppv, recall, f_score)
                
def plot_bar_graph(before_data, after_data, quantity, out_file):
    before_xs = [i - 0.2 for i in range(0, len(before_data))]
    after_xs = [i + 0.2 for i in range(0, len(before_data))]

    figure, axes = plot.subplots()
    figure.set_size_inches(16, 8)
    figure.dpi = 400
    
    plot.bar(before_xs, before_data, 0.4, label= "Pre-filtering " + quantity, color= [0,116/255,217/255])
    plot.bar(after_xs, after_data, 0.4, label= "Post-filtering " + quantity, color= [255/255,133/255,27/255])
    
    plot.xticks([i for i in range(0, len(before_data))], ["S" + str(i+1) for i in range(0, len(before_data))])
    
    plot.xlabel("Sample")
    plot.ylabel(quantity.capitalize())
    plot.title(quantity.capitalize() + " Before and After Filtering \n(strand bias + 2% min af + 5% min af change + simple deletion removal)")
    plot.legend()
    
    plot.savefig(out_file)
                
def run():
    parser = argparse.ArgumentParser(description='Plots the precision, recall, and f-score before and after vcf processing')
    parser.add_argument('input', help='the input vcf directory')
    parser.add_argument('--meta', dest= "metadata", help='the input vcf directory')
    parser.add_argument('--unfiltered', dest= "unfiltered", help='the unfiltered vcf directory')

    args = parser.parse_args()
    vcf_dir = args.input
    original_vcf_dir = args.unfiltered
    metadata_file = args.metadata
    
    before_data = []
    after_data = []
        
    with open(metadata_file) as csvfile:
        for row in csv.DictReader(csvfile):
            illumina_id = row["illumina_id"]
            nanopore_id = row["nanopore_id"]
        
            if illumina_id == "." or nanopore_id == ".":
                continue
        
            illumina_file = os.path.join(original_vcf_dir, illumina_id + ".vcf")
            nanopore_file = os.path.join(original_vcf_dir, nanopore_id + ".vcf")
            nanopore_filtered = os.path.join(vcf_dir, nanopore_id + ".vcf")
            
            before_data.append(calculate_ppv_recall(illumina_file, nanopore_file))
            after_data.append(calculate_ppv_recall(illumina_file, nanopore_filtered))
    
    before_ppvs = [pair[0] for pair in before_data]
    after_ppvs = [pair[0] for pair in after_data]
    
    before_recalls = [pair[1] for pair in before_data]
    after_recalls = [pair[1] for pair in after_data]
    
    before_fs = [pair[2] for pair in before_data]
    after_fs = [pair[2] for pair in after_data]
    
    plot_bar_graph(before_ppvs, after_ppvs, "precision", "./precision.png")
    plot_bar_graph(before_recalls, after_recalls, "recall", "./recall.png")
    plot_bar_graph(before_fs, after_fs, "f-score", "./f_score.png")
    
run()
