#!/usr/bin/python

# plot_time_scatter.py
# Written by Joshua Kearney

import sys
import argparse
import os
import shutil
import vcf
import csv

import matplotlib.pyplot as plot

from typing import NamedTuple
from collections import defaultdict
from datetime import datetime
from multiprocessing import Pool
from colour import Color
from Bio import SeqIO
from Bio.Seq import Seq

class VariantIdentity(NamedTuple):
    pos: int = 0
    ref: str = 0
    alt: str = 0
    
class VariantInfo:
    nanopore_count: int = 0
    illumina_count: int = 0
    af_total: int = 0
    
class ScatterPlotInfo:
    xs = []
    ys = []
    sizes = []
    afs = []
    colors = []
    count = 0
    
    def __init__(self, x, y, s, af, c):
        self.xs = x
        self.ys = y
        self.sizes = s
        self.afs = af
        self.count = c
        self.colors = []
        
def get_sample_pairs(metadata_file):
    with open(metadata_file, "r") as file:
        reader = csv.DictReader(file)
        samples = {}
        
        for row in reader:
            date = int(row["timepoint"])
            nanopore_id = row["nanopore_id"].strip()
            illumina_id = row["illumina_id"].strip()
        
            if nanopore_id != "." and illumina_id != ".":
                samples[date] = (nanopore_id, illumina_id)
        
        dates = [date for date in samples.keys()]
        dates.sort()
        
        sample_pairs = []
        for date in dates:
            sample_pairs.append(samples[date])
                
        return sample_pairs
    
def get_variants(file_names, illumina_names):
    vars = defaultdict(lambda: VariantInfo())

    for vcf_file in file_names:
        reader = vcf.Reader(open(vcf_file, "r"))
        
        for record in reader:
            name = os.path.splitext(os.path.basename(vcf_file))[0].split(".")[0]
            pos = record.POS
            ref = str(record.REF)
            af = record.INFO["AF"]
            
            # print(name + " | " + str(illumina_names))
            
            if record.is_indel:
                continue
            
            for alt in [str(x) for x in record.ALT]:
                id = VariantIdentity(pos, ref, alt)
                vars[id].af_total += af
                
                if name in illumina_names:
                    vars[id].illumina_count += 1
                else:
                    vars[id].nanopore_count += 1
                    
    return vars
    
def get_timed_variants(var_dir, sample_pairs):
    illumina_names = [pair[1] for pair in sample_pairs]
    var_list = []
        
    for (nanopore_id, illumina_id) in sample_pairs:
        nanopore_file = os.path.join(var_dir, nanopore_id + ".vcf")
        illumina_file = os.path.join(var_dir, illumina_id + ".vcf")
        vars = get_variants([nanopore_file, illumina_file], illumina_names)
        
        var_list.append(vars)
    
    return var_list

def plot_variants(timed_vars, out_file):
    nanopore_counts = defaultdict(int)
    illumina_counts = defaultdict(int)
    
    for vars in timed_vars:
        for (id, data) in vars.items():
            nanopore_counts[id] += data.nanopore_count
            illumina_counts[id] += data.illumina_count
    

    blue = [0/255, 116/255, 217/255]
    red = [255/255, 65/255, 54/255]
    purple = [177/255, 13/255, 201/255]

    total = int(len(timed_vars))
    blues = [blue]
    purples = [purple]
    reds = [red]
    
    for i in range(0, total):
        last = blues[-1]
        next = Color(rgb= last)
        next = (next.hue, max(0, next.saturation * 0.96), min(1, next.luminance * 1.04))
        next = Color(hsl= next).rgb
        next = list(next)
        
        blues.append(next)
        
    for i in range(0, total):
        last = purples[-1]
        next = Color(rgb= last)
        next = (next.hue, max(0, next.saturation * 0.96), min(1, next.luminance * 1.04))
        next = Color(hsl= next).rgb
        next = list(next)
        
        purples.append(next)
        
    for i in range(0, total):
        last = reds[-1]
        next = Color(rgb= last)
        next = (next.hue, max(0, next.saturation * 0.96), min(1, next.luminance * 1.04))
        next = Color(hsl= next).rgb
        next = list(next)
        
        reds.append(next)

    blues.reverse()
    purples.reverse()
    reds.reverse()

    alpha = 1
    count = 0

    figure, axes = plot.subplots()
    figure.set_size_inches(8, 8)
    figure.dpi = 600

    for vars in timed_vars:
        alpha -= 0.7/len(timed_vars)
        count += 1
    
        both_info = ScatterPlotInfo([], [], [], [], 0)
        nanopore_info = ScatterPlotInfo([], [], [], [], 0)
        illumina_info = ScatterPlotInfo([], [], [], [], 0)
            
        # Get plot data
        for (id, data) in vars.items():
            total = data.illumina_count + data.nanopore_count
            y = data.af_total / total
            size = (nanopore_counts[id] + illumina_counts[id]) / 2
            color = None
            info = None
            
            #if nanopore_counts[id] < 2 or illumina_counts[id] < 2:
            #    continue
            
            # print(str(data.nanopore_count) + " | " + str(data.illumina_count))
            
            if data.nanopore_count != 0 and data.illumina_count != 0:
                info = both_info
                color = purples[count]
            elif data.nanopore_count != 0:
                info = nanopore_info
                color = reds[count]
            else:
                info = illumina_info
                color = blues[count]
                
            info.xs.append(id.pos)
            info.ys.append(y)
            info.sizes.append(size)
            info.afs.append(data.af_total)
            info.count += total
            info.colors.append(color)


        # Plot the data on three scatter plots
        scatter1 = axes.scatter(both_info.xs, both_info.ys, s= both_info.sizes, c= both_info.colors, label= "Nanopore and illumina")
        scatter2 = axes.scatter(nanopore_info.xs, nanopore_info.ys, s= nanopore_info.sizes, c= nanopore_info.colors, label= "Nanopore only")
        scatter3 = axes.scatter(illumina_info.xs, illumina_info.ys, s= illumina_info.sizes, c= illumina_info.colors, label= "Illumina only")
    
    plot.title("Change in Allele Frequency Through Time")
    plot.xlabel("Position")
    plot.ylabel("Frequency (2% min)")
    
    figure.subplots_adjust(bottom= 0.35)

    plot.savefig(out_file)

    
def run():
    parser = argparse.ArgumentParser(description='Plots a scatter plot over time of the given variants showing position, allele frequency, number of supporting samples, and sequencing technology.')
    parser.add_argument('input', help='the input vcf directory')
    parser.add_argument('--meta', dest= "metadata", help='the input vcf directory')
    parser.add_argument('--out', dest= "output", help='the output png file')

    args = parser.parse_args()
    input_dir = args.input
    out_file = args.output
    metadata_file = args.metadata
    
    pairs = get_sample_pairs(metadata_file)
    vars = get_timed_variants(input_dir, pairs)
        
    plot_variants(vars, out_file)
    
run()
