#!/usr/bin/python

# filter_all.py
# Written by Joshua Kearney

import sys
import argparse
import os
import shutil
import itertools
import csv
import shutil

from multiprocessing import Pool
from collections import defaultdict
from typing import NamedTuple
from typing import Dict

from Bio import SeqIO
from Bio.Seq import Seq
    
def get_name(path):
    return os.path.splitext(os.path.basename(path))[0].split(".")[0].split("_")[0]
    
def filter_af(vcf_dir, meta_file, out_dir):
    if os.path.exists(out_dir):
        shutil.rmtree(out_dir)
    
    os.makedirs(out_dir)

    command = "python filter_af_change.py --out {output} --meta {meta} --min-af-change 0.05 --min-samp-count 1 --safe-samp-cov 0.65 {input}"
    command = command.format(input= vcf_dir, output= out_dir, meta= meta_file)
    os.system(command)
    print("Samples filtered by allele frequency change")
        
def filter_entropy(vcf_file, ref_file, out_dir):
    name = get_name(vcf_file)
    out_file = os.path.join(out_dir, name + ".vcf")
        
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
        
    # Filter variants with lofreq
    command = "python filter_entropy.py --out {output} --ref {ref} {input}"
    command = command.format(input= vcf_file, output= out_file, ref= ref_file)
    os.system(command)
    print("Sample '" + name + "' filtered by entropy")
    
    return out_file
                    
def run():
    parser = argparse.ArgumentParser(description='Applies a low-complexity deletion filter and an allele frequency change filter to the vcf files in the given directory')
    parser.add_argument("input", help='The input vcf directory')
    parser.add_argument('--out', dest= 'output', help='The output vcf directory')
    parser.add_argument('--ref', dest= 'ref', help='The reference genome to use')
    parser.add_argument('--meta', dest= 'metadata', help='The metadata file to use')

    args = parser.parse_args()
    ref_file = args.ref
    vcf_dir = args.input
    out_dir = args.output
    meta = args.metadata
        
    filter_af(vcf_dir, meta, "./temp/vcf_filtered_af/")
        
    illumina_ids = set()
    nanopore_ids = set()
        
    with open("metadata.csv") as csvfile:
        for row in csv.DictReader(csvfile):
            illumina_id = row["illumina_id"]
            nanopore_id = row["nanopore_id"]
        
            if illumina_id != ".":
                illumina_ids.add(illumina_id)
        
            if nanopore_id != ".":
                nanopore_ids.add(nanopore_id)
    
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    
    for id in illumina_ids:
        dest_file = os.path.join(out_dir, id + ".vcf")
        src_file = os.path.join(vcf_dir, id + ".vcf")
                
        shutil.copyfile(src_file, dest_file)
        
    for id in nanopore_ids:
        src_file = os.path.join("./temp/vcf_filtered_af/", id + ".vcf")
        
        filter_entropy(src_file, ref_file, out_dir)
        
    print("Done!")
    
run()
