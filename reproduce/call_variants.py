#!/usr/bin/python

# call_variants.py
# Written by Joshua Kearney

import sys
import argparse
import os
import shutil
import itertools
import csv

from multiprocessing import Pool
from collections import defaultdict
from typing import NamedTuple
from typing import Dict

from Bio import SeqIO
from Bio.Seq import Seq
    
def get_name(path):
    return os.path.splitext(os.path.basename(path))[0].split(".")[0].split("_")[0]
    
def index_fasta(fasta_file):
    if not os.path.exists(fasta_file + ".fai"):
        command = "lofreq faidx {input}"
        command = command.format(input= fasta_file)
        os.system(command)
        print("Indexed reference '" + fasta_file + "'")
    
def insert_bam_indelquals_dindel(bam_file, ref_file, threads, bam_dir):
    name = get_name(bam_file)
    out_file = os.path.join(bam_dir, name + ".bam")
    
    if os.path.exists(out_file):
        return out_file
        
    if not os.path.exists(bam_dir):
        os.makedirs(bam_dir)

    # Insert indel qualities with lofreq
    command = "lofreq indelqual --dindel --ref {ref} --out {output} {input}"
    command = command.format(input= bam_file, output= out_file, ref= ref_file)
    os.system(command)
    print("Inserted indel qualities for sample " + name)
    
    return out_file
    
def insert_bam_indelquals_uniform(bam_file, ref_file, threads, average_phred, bam_dir):
    name = get_name(bam_file)
    out_file = os.path.join(bam_dir, name + ".bam")
    
    if os.path.exists(out_file):
        return out_file
        
    if not os.path.exists(bam_dir):
        os.makedirs(bam_dir)

    # Insert indel qualities with lofreq
    command = "lofreq indelqual --uniform {phred} --ref {ref} --out {output} {input}"
    command = command.format(input= bam_file, output= out_file, ref= ref_file, phred= average_phred)
    os.system(command)
    print("Inserted indel qualities for sample " + name)
    
    return out_file

def call_variants(bam_file, ref_file, threads, out_dir):
    name = get_name(bam_file)
    out_file = os.path.join(out_dir, name + ".vcf")
    
    if os.path.exists(out_file):
        return out_file
        
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    # Index this bam file with lofreq
    command = "lofreq index {input}"
    command = command.format(input= bam_file)
    os.system(command)
    print("Bam indexed for sample " + name)
    
    # Call variants with lofreq
    command = "lofreq call-parallel --pp-threads {threads} --ref {ref} --out {output} {input} --call-indels --no-default-filter --verbose"
    command = command.format(threads= threads, ref= ref_file, output= out_file, input= bam_file)
    os.system(command)
    print("Variants called for '" + name + "'")
    
    return out_file
    
def filter_variants(vcf_file, out_dir):
    name = get_name(vcf_file)
    out_file = os.path.join(out_dir, name + ".vcf")
    
    if os.path.exists(out_file):
        return out_file
        
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
        
    # Filter variants with lofreq
    command = "lofreq filter --in {input} --out {output} --cov-min 2 --af-min 0.02 --sb-alpha 0.01 --sb-incl-indels --verbose"
    command = command.format(input= vcf_file, output= out_file)
    os.system(command)
    print("Variants filtered")
    
    return out_file

def call_snps_lofreq_nanopore(bam_file, ref_file, threads):
    index_fasta(ref_file)
    bam_file = insert_bam_indelquals_uniform(bam_file, ref_file, threads, 16, "./temp/bam_indelquals/")
    vcf = call_variants(bam_file, ref_file, threads, "./temp/vcf_unfiltered/")
    vcf = filter_variants(vcf, "./vcf/")
    
def call_snps_lofreq_illumina(bam_file, ref_file, threads):
    index_fasta(ref_file)
    bam_file = insert_bam_indelquals_dindel(bam_file, ref_file, threads, "./temp/bam_indelquals/")
    vcf = call_variants(bam_file, ref_file, threads, "./temp/vcf_unfiltered/")
    vcf = filter_variants(vcf, "./vcf/")
                    
def run():
    parser = argparse.ArgumentParser(description='Call covid time-series data with lofreq')
    parser.add_argument("input", help='The input bam directory')
    parser.add_argument('--threads', dest= 'threads', type=int, default=1, help='the number of threads to use')
    parser.add_argument('--ref', dest= 'ref', help='the reference genome to use')
    parser.add_argument('--meta', dest= 'metadata', help='the read metadata to use in csv format')

    args = parser.parse_args()
    threads = args.threads
    ref = args.ref
    bam_dir = args.input
        
    illumina_ids = set()
    nanopore_ids = set()
    file_paths = {}
        
    with open("metadata.csv") as csvfile:
        for row in csv.DictReader(csvfile):
            illumina_id = row["illumina_id"]
            nanopore_id = row["nanopore_id"]
        
            if illumina_id != ".":
                illumina_ids.add(illumina_id)
        
            if nanopore_id != ".":
                nanopore_ids.add(nanopore_id)
                
    for file_path in [triple[2] for triple in os.walk(bam_dir)][0]:
        id = get_name(file_path)
        file_paths[id] = os.path.join(bam_dir, file_path)
    
    for id in illumina_ids:
        bam_file = file_paths[id]
        call_snps_lofreq_illumina(bam_file, ref, threads)
        
    for id in nanopore_ids:
        bam_file = file_paths[id]
        call_snps_lofreq_nanopore(bam_file, ref, threads)
        
    print("Done!")
    
run()
