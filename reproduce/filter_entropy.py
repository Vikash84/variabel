#!/usr/bin/python

# filter_entropy.py
# Written by Yunxi Liu and Joshua Kearney

import sys
import argparse
import os
import shutil
import vcf

from multiprocessing import Pool
from collections import defaultdict
from scipy.stats import entropy

from Bio import SeqIO
from Bio.Seq import Seq

# calculating Shannon entropy of a string
def string_entropy(input_string):
    char_dict = defaultdict(int)
    for char in input_string:
        char_dict[char] += 1
    pk = []
    for char in char_dict:
        pk.append(char_dict[char]/len(input_string))
    return entropy(pk)
    
# filter a deletion by its size and pos
def deletion_complexity_filter(record, ref_sequence, k = 3, min_entropy_cutoff = 1):
    if not record.is_indel:
        return True

    # position of the reference base
    pos = record.POS - 1
    # length of the deletion
    deletion_length = len(record.REF) - 1
    # check k*length of deletion bases on fwd direction, exclude ref base
    fwd_extend = ref_sequence[pos+1: pos+1+deletion_length*k]
    # check (k-1)*length of deletion bases on rev direction, puls ref base
    rev_extend = ref_sequence[pos-deletion_length*(k-1): pos+1]
    if string_entropy(fwd_extend) * string_entropy(rev_extend) >= min_entropy_cutoff:
        return True
        
    return False

def filter_vcf(input, output, ref):
    reader = vcf.Reader(open(input, "r"))
    writer = vcf.Writer(open(output, "w"), reader)

    records = list(reader)
    records =  filter(lambda r: deletion_complexity_filter(r, ref), records)

    for record in records:
        writer.write_record(record)
    
def run():
    parser = argparse.ArgumentParser(description='Removes low-complexity indels from the given vcf file')
    parser.add_argument('input', help='The input fastq file to start the pipeline')
    parser.add_argument('--out', dest= 'output', help='The output vcf filename')
    parser.add_argument('--ref', dest= 'ref', help='The reference genome')

    args = parser.parse_args()
    ref = str(list(SeqIO.parse(args.ref, "fasta"))[0].seq)
    
    filter_vcf(args.input, args.output, ref)
    
run()
